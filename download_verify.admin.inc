<?php
/**
 * @file
 * Generate configuration form and save settings.
 */

/**
 * Form Builder.
 *
 * @ingroup forms.
 * @see system_settings_form()
 */
function download_verify_configuration_form($form, &$form_state) {
  // Get the path to the module.
  $path = drupal_get_path('module', 'download_verify');
  // @todo: Add Honeypot protection, is the module available?.
  // @see https://drupal.org/project/honeypot
  // @todo: honeypot_add_form_protection($form, $form_state, array('honeypot', 'time_restriction'));

  // Get the module variables
  $options = variable_get('dvsettings');
  if (isset($options['download_verify_mail_settings'])) {
    // Data format follows expected arrangement
    $dv_email = $options['download_verify_mail_settings']['download_verify_email'];
    $dv_target = $options['download_verify_css_target_settings']['download_verify_css_target'];
    $dv_intro = $options['download_verify_intro_text_settings']['download_verify_intro_text'];
    $dv_footer = $options['download_verify_footer_text_settings']['download_verify_footer_text'];
    $dv_display = $options['download_verify_cookie_settings']['download_verify_cookie_display'];
    $dv_expire = $options['download_verify_cookie_settings']['download_verify_cookie_expiry'];
  }
  else {
    // Mangled array from upgraded data, deal with it, fix on next save.
    $dv_email = $options[0];
    $dv_target = $options[1];
    $dv_intro = $options[2];
    $dv_footer = $options[3];
    $dv_display = $options[4];
    $dv_expire = $options[5];
  }

  // Attach the CSS to the form.
  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => $path . '/css/download_verify.css',
    ),
  );

  // Set up the form container to group configuration values in database.
  $form['dvsettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Download Verify configuration'),
    '#description' => t('These configurations will target a specific list format within your sites pages or blocks.'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  // Set up the mail field container.
  $form['dvsettings']['download_verify_mail_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail configuration'),
    '#description' => t('Enter the e-mail address to be used for form submission configuration. This e-mail address will not be exposed to the site visitor.'),
    '#weight' => 1,
    '#collapsible' => FALSE,
  );

  // Single text field for email address entry.
  $form['dvsettings']['download_verify_mail_settings']['download_verify_email'] = array(
    '#type' => 'textfield',
    '#default_value' => $dv_email,
    '#title' => t('E-mail address where enquiries are sent to.'),
    '#weight' => 2,
    '#collapsible' => FALSE,
  );

  // Set up the description for the CSS reference container.
  // HTML used to demonstrate code structure for less experienced end users.
  $download_verify_css_target_settings_description = '<p>' . t('Enter the CSS class name to be used for attaching the form to.') . '<p><p>' . t('This can be any&nbsp;') . '<em>&nbsp;' . t('wrapper') . '</em>&nbsp;' . t('element that contains any number of HTML anchor tags. See the modules&nbsp;') . l(t('help page'), '../../help/download_verify') . t('for further information.');

  // Set up the CSS reference container.
  $form['dvsettings']['download_verify_css_target_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS class name target configuration'),
    '#description' => filter_xss_admin($download_verify_css_target_settings_description),
    '#weight' => 3,
    '#collapsible' => FALSE,
  );

  // Single text field for CSS target reference entry.
  $form['dvsettings']['download_verify_css_target_settings']['download_verify_css_target'] = array(
    '#type' => 'textfield',
    '#default_value' => $dv_target,
    '#title' => t('CSS class name.'),
    '#weight' => 4,
    '#collapsible' => FALSE,
  );

  // Form element for setting intro text.
  // Set up the intro text container.
  $form['dvsettings']['download_verify_intro_text_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Intro text content'),
    '#description' => t('Enter the text you wish to be shown at the top of the form'),
    '#weight' => 5,
    '#collapsible' => FALSE,
  );

  // Textarea field for intro text entry.
  $form['dvsettings']['download_verify_intro_text_settings']['download_verify_intro_text'] = array(
    '#type' => 'textarea',
    '#default_value' => $dv_intro,
    '#title' => t('The text to display'),
    '#weight' => 6,
    '#rows' => 3,
    '#collapsible' => FALSE,
  );

  // Form element for setting footer text.
  // @todo: needs placeholder for link to privacy policy.
  $form['dvsettings']['download_verify_footer_text_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer text content'),
    '#description' => t('Enter the text you wish to be shown at the bottom of the form'),
    '#weight' => 7,
    '#collapsible' => FALSE,

  );

  // Textarea field for footer text entry.
  $form['dvsettings']['download_verify_footer_text_settings']['download_verify_footer_text'] = array(
    '#type' => 'textarea',
    '#default_value' => $dv_footer,
    '#title' => t('The text to display'),
    '#weight' => 8,
    '#collapsible' => FALSE,
    '#rows' => 6,
  );

  // Set up fieldset for cookie options.
  $form['dvsettings']['download_verify_cookie_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cookie options'),
    '#description' => t('Configurations to allow the setting of the cookie and its expiry date/time. Ensure you update any site usage policies to reflect changes made here.'),
    '#weight' => 9,
    '#collapsible' => FALSE,
  );

  // Set up radio buttons for optional cookie setting.
  $form['dvsettings']['download_verify_cookie_settings']['download_verify_cookie_display'] = array(
    '#type' => 'radios',
    '#title' => t('Set cookie on form submission.'),
    '#default_value' => $dv_display,
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
    ),
    '#description' => t('Optional setting of the cookie on the users system. When this is set to <em>Off</em> then the module will not check for the presence of previous cookies either.'),
    '#weight' => 10,
    '#collapsible' => FALSE,
  );
  // Declare the cookie expiry options.
  $download_verify_cookie_expiry_values = drupal_map_assoc(array(
    86400,
    604800,
    2419200,
    7776000,
    15552000,
    31104000,
  ), 'format_interval');
  // Set up the select element for the cookie expiry time.
  $form['dvsettings']['download_verify_cookie_settings']['download_verify_cookie_expiry'] = array(
    '#type' => 'select',
    '#title' => 'Cookie expiry time.',
    '#default_value' => $dv_expire,
    '#options' => $download_verify_cookie_expiry_values,
    '#description' => t('Set the expiry period for the cookie that is set on the users system.'),
    '#weight' => 11,
    '#collapsible' => FALSE,
  );
  // @todo: Additional textarea element for setting the body of the email sent.
  // @todo: Use placeholder tokens for details.
  return system_settings_form($form);
}
